var express = require('express');
var app = express();
var bone = require('bonescript');

app.set('view engine','jade');
app.set('views','www/html');
app.use(express.static('www'));

app.get('/pot',function(req,res){
    bone.analogRead('P9_40',function(pot){
        pot = (pot-0.5)*2;
        res.json({
            pot: pot
        });
    });//Math.random();
});

app.get('/',function(req,res){
    res.render('GameScreen');
});

var server = app.listen(420,function(){
	console.log('Server listening on port 420');
});