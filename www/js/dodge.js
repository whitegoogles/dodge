$(document).ready(function(){
    var asteroids = [];
    var asteroidRowSize = 10;
    var asteroidSpacing = 150;
    var cnvSize = 800;
    var asteroidSize = 30;
    var player = {
        size: 15,
        color: "#319432",
        speed: 30
    };
    player.x = cnvSize/2 - player.size/2;
    player.y = cnvSize-player.size;
    function genAsteroids(num){
        var asteroids = [];
        var i;
        for(i = 0; i<num; i+=1){
            var xSpot = Math.random()*(cnvSize-asteroidSize);
            var ySpot = -asteroidSize;
            asteroids.push({x:xSpot,y:ySpot});
        }
        return asteroids;
    };
    var i;
    asteroids = genAsteroids(asteroidRowSize);
    function moveAsteroids(){
        var i;
        var asteroidSpeed = 3;
        var newAsteroids = [];
        for(i = 0; i<asteroids.length;i+=1){
            asteroids[i].y+=asteroidSpeed;
            if(asteroids[i].y<cnvSize){
                newAsteroids.push(asteroids[i]);
            }
            if(asteroids[i].y === asteroidSpacing && i=== asteroids.length-1){
                newAsteroids = newAsteroids.concat(genAsteroids(asteroidRowSize));
            }
            if((asteroids[i].y<player.y && asteroids[i].y+asteroidSize>player.y)||
               (asteroids[i].y<player.y+player.size && asteroids[i].y+asteroidSize>player.y+player.size)){
                if((asteroids[i].x<player.x && asteroids[i].x+asteroidSize>player.x)||
                   (asteroids[i].x<player.x+player.size && asteroids[i].x+asteroidSize>player.x+player.size)){
                        player.hit = true;
                   }
            }
        }
        asteroids = newAsteroids;
    };
    function movePlayer(){
        $.ajax('/pot',{
            success: function(resp){
                player.x+=resp.pot*player.speed;
                if(player.x>cnvSize){
                    player.x = -player.size;
                }
                if(player.x<-player.size){
                    player.x = cnvSize;
                }
            },
            error: function(err){       
            }
        });
    };
    function render(){
        var cnv = document.getElementById("cnv");
        var ctx = cnv.getContext("2d");
        if(!player.hit){
            ctx.fillStyle = "#d2b48c";
            ctx.fillRect(0,0,cnvSize,cnvSize);
            ctx.fillStyle = "#8B8989";
            for(i = 0; i<asteroids.length; i+=1){
                ctx.fillRect(asteroids[i].x,asteroids[i].y,asteroidSize,asteroidSize);
            }
            ctx.fillStyle = player.color;
            ctx.fillRect(player.x,player.y,player.size,player.size);
        }
        else{
            ctx.fillStyle = "#000000";
            ctx.fillRect(0,0,cnvSize,cnvSize);
            ctx.fillStyle = "#FFFFFF";
            ctx.font = "30px Arial";
            var gameOverText = "Game Over :^ (";
            var textProps = ctx.measureText(gameOverText);
            ctx.fillText(gameOverText,cnvSize/2-textProps.width/2,cnvSize/2-31/2);
        }
    };
    setInterval(render,50);
    setInterval(movePlayer,50);
    setInterval(moveAsteroids,50);
});